import {Component, Input, OnInit} from '@angular/core';
import {EventSponsorModel} from '../../events/network/models/event.sponsor.model';
import {EventsService} from '../../events/network/services/events.service';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.css']
})
export class SponsorsComponent implements OnInit{

  @Input() isRTL : any;

  @Input()
    eventId:number;

  slideConfig = {
    slidesToShow: 5,
    slidesToScroll:4,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll:1
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll:2
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll:1
        }
      }
    ]
  };
  eventSponsor : EventSponsorModel[];

  constructor(private eventService: EventsService) { }

  ngOnInit() {
    this.eventService.getEventSponsor(this.eventId).subscribe(value => {
      this.eventSponsor = value;
    });
  }



}
