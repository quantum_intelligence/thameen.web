import {Component, Input, OnInit,} from '@angular/core';
import {EventModel} from '../../events/network/models/event.model';
import {UserSettingService} from '../../../_helpers/user.setting.service';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.css']
})
export class EventCardComponent implements OnInit {

  @Input() eventmodel : EventModel;

  @Input() index   : any;

  @Input() currency : string;

  @Input()
  eventid: any;

  textdirc : string = "left";

  constructor(private userSettingService: UserSettingService) {
    var lang = userSettingService.getLang();
    if(lang == 'ar'){
      this.textdirc = "right";
    }else {
      this.textdirc = "left";
    }
  }

  ngOnInit() {
  }

  formatDate(stringDate:any) {
    if(this.userSettingService.getLang() == 'ar'){
      var monthes = ['none' , 'يناير' , 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو','يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'];
     var parsedDate = stringDate.split(' ')[0].split('/');
    var  monthOnDate = parsedDate[1] as number;
     console.log(monthOnDate);

      return parsedDate[0] + " " + monthOnDate + " " + parsedDate[2];
    }else {
      var date = new Date(stringDate);
      var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }
  }

   truncateText(text: any) {

   var truncated = text;

    if(truncated !=null){
      if (truncated.length > 200) {
        truncated = truncated.substr(0,100) + '...';
      }
    }
    return truncated;
  }


}
