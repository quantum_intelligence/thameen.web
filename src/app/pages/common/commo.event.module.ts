import { NgModule } from '@angular/core';
import {EventCardComponent} from './event-card/event-card.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {SponsorsComponent} from './sponsors/sponsors.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import { EventListTypeComponent } from './event-list-type/event-list-type.component';
import {EventsFilterComponent} from './events-filter/events-filter.component';
import {FormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {HttpLoaderFactory} from '../../app.module';

@NgModule({
  declarations: [EventCardComponent , SponsorsComponent, EventListTypeComponent , EventsFilterComponent],
  imports: [
    CommonModule , SharedModule, RouterModule , SlickCarouselModule , FormsModule , TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ] ,
  exports: [EventCardComponent , SponsorsComponent , EventListTypeComponent , EventsFilterComponent]
})
export class CommoEventModule { }
