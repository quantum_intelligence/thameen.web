import {Component, Input, OnInit} from '@angular/core';
import {EventModel} from '../../events/network/models/event.model';

@Component({
  selector: 'app-event-list-type',
  templateUrl: './event-list-type.component.html',
  styleUrls: ['./event-list-type.component.css']
})
export class EventListTypeComponent implements OnInit {

  @Input() eventmodel : EventModel;

  @Input() index   : any;

  @Input() currency : string;

  @Input()
  eventId: any;

  constructor() { }

  ngOnInit() {
  }

   formatDate(stringDate:any) {
    var date = new Date(stringDate);
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

}
