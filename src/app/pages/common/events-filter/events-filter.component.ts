import { Component, OnInit } from '@angular/core';
import {EventModel} from '../../events/network/models/event.model';
import {EventsService} from '../../events/network/services/events.service';
import {EventTypeModel} from '../../events/network/models/event.type.model';
import {EventSpecialityModel} from '../../events/network/models/event.speciality.model';

@Component({
  selector: 'app-events-filter',
  templateUrl: './events-filter.component.html',
  styleUrls: ['./events-filter.component.css']
})
export class EventsFilterComponent implements OnInit {

  eventTypes : EventTypeModel[];
  specilities : EventSpecialityModel[];
  eventtypeId : number;
  specialityId: number;
  fromdate: Date;
  todate: Date;
  events = new Array<EventModel>();
  isEmptydata:boolean;
  isLoading: boolean;

  constructor(private eventService: EventsService) {

    this.loadEventsType();
    this.loadEventsSpecialities();
    this.isLoading = true ;
    this.isEmptydata = false;
    this.loadEvents();
  }


  ngOnInit() {
  }

  loadEvents(){
    this.eventService.getEvents().subscribe(value => {
      this.events = value;
      this.isLoading = false;
      if(value.length ==0){
        this.isEmptydata = true;
      }
    });
  }

  loadEventsType(){
    this.eventService.getEventsType().subscribe(value => {
      this.eventTypes = value;
    });
  }

  loadEventsSpecialities(){
    this.eventService.getEventsSpecialities().subscribe(value => {
      this.specilities = value;
    });
  }

  searchForEvents(){
    this.isLoading = true;
    this.isEmptydata = false;
    console.log(this.todate);
    console.log(this.fromdate);
    this.eventService.getEventByFilter(this.getFormatedDate(this.fromdate) , this.getFormatedDate(this.todate) , this.specialityId , this.eventtypeId).subscribe(value => {
      this.events = value;
      this.isLoading = false;
      if(value.length==0){
        this.isEmptydata = true;
      }
    });

  }

  getFormatedDate(date:Date){
    if(date){
      var month = date.getMonth() + 1;
      return date.getDate() + "-" + month + "-" + date.getFullYear();
    }
  }

  clear(){
    this.fromdate = null;
    this.todate = null;
    this.specialityId = null;
    this.eventtypeId = null;
  }

}
