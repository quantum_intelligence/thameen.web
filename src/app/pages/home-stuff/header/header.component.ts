import { Component, OnInit } from '@angular/core';
import {EmbryoService} from '../../../service/Embryo.service';
import {CustomAuthService} from '../../../anonymous/networks/custom.auth.service';
import {UserProfileService} from '../../../_helpers/user.profile.service';
import {AccountModel} from '../../account/networks/account.model';
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  accountModel: AccountModel = new AccountModel();
  imagepath:string = 'assets/images/white_logo.png';

  constructor(public embryoService: EmbryoService , public loginService:CustomAuthService , private userprofileService: UserProfileService) {
    this.accountModel = this.userprofileService.getProfile();
  }

  ngOnInit() {
    if(this.accountModel != null){
     if(this.accountModel.imagePath !=null){
       this.imagepath = this.accountModel.imagePath;
     }
    }
  }

  public toggleSidebar()
  {
    this.embryoService.sidenavOpen = !this.embryoService.sidenavOpen;
  }

  public toggleSearch() {
    $('app-main').toggleClass('form-open');
  }

  logout(){
    if(this.loginService.islogedIn()){
      this.loginService.logout();
    }
  }

}
