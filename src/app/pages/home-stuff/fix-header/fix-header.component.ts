import { Component, OnInit } from '@angular/core';
import {EmbryoService} from '../../../service/Embryo.service';

@Component({
  selector: 'app-fix-header',
  templateUrl: './fix-header.component.html',
  styleUrls: ['./fix-header.component.css']
})
export class FixHeaderComponent implements OnInit {

  constructor(private embryoService : EmbryoService) { }

  ngOnInit() {
  }

  public toggleSidebar()
  {
    this.embryoService.sidenavOpen = !this.embryoService.sidenavOpen;
  }

}
