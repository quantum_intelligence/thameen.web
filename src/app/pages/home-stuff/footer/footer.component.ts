import { Component, OnInit } from '@angular/core';
import {MenuItems} from '../menu/menu-items';
import {UserSettingService} from '../../../_helpers/user.setting.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  textdirc:string = "left";

  constructor(public menuItems : MenuItems , private userSettingService: UserSettingService) {
    if(userSettingService.getLang() == 'ar'){
       this.textdirc = "right";
    }else{
      this.textdirc = "left";
    }
  }

  ngOnInit() {
  }

  public ishref(link:string){
    return link.startsWith("h");
  }

}
