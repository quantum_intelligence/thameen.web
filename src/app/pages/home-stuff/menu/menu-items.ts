import { Injectable } from '@angular/core';

/*
 * Menu interface
 */
export class Menu {
	state: string;
	name?: string;
	type?: string;
	icon?: string;
	ishref?:boolean;
	children?: Menu[];


}

const HeaderOneItems= [
  {
    state: "",
    name: "menus.home",
    icon: "home",
    type: "link"
  },
  {
    state: "/events/me",
    name : "menus.my_event",
    type: "link",
    icon: "list_alt",

  },
/*  {
    state: "/",
    name : "menus.administration",
    type: "link",
    icon: "important_devices",
    children: [
      { state: "http://admin.motamarat.live:81",
        name : "menus.admin",
        type: "link",
        icon: "important_devices" , openNew: true} ,

      { state: "http://admin.motamarat.live:81",
        name : "menus.event_manager",
        type: "link",
        icon: "important_devices"} ,
      { state: "/",
        name : "menus.speaker",
        type: "link",
        icon: "important_devices"}]
  }*/
  {
    state: "/about",
    name : "menus.about_us",
    type: "link",
    icon: "error_outline",

  } ,
  {
    state: "/contact",
    name : "menus.contact_us",
    type: "link",
    icon: "contact_mail",

  }
];


const FooterMenus= [
  {
    state: "",
    name: "footer.your_account",
    icon: "home",
    type: "link" ,
    children:[{
      name: "footer.signup",
      state: "/register",
      icon: "home",
      type: "link"
    } , {
      name: "footer.login" ,
      state: "/login",
      icon: "home",
      type: "link"
    } , {
      name: "footer.forget_password",
      state: "/password/forget",
      icon: "home",
      type: "link"
    } , {
      name: "footer.help",
      state: "/contact",
      icon: "home",
      type: "link"
    }]
  }
  ,
  {
    state: "/",
    name : "footer.admin",
    type: "link",
    icon: "important_devices",
    children: [
      {
        state: "https://admin-motamarat.kfshrc.edu.sa",
        name : "footer.admin_only",
        type: "link",
        icon: "important_devices" , isherf: true } ,

      { state: "https://admin-motamarat.kfshrc.edu.sa",
        name : "footer.event_manager",
        type: "link",
        icon: "important_devices" , isherf: true} ,
      {
        state: "https://speaker-motamarat.kfshrc.edu.sa",
        name : "footer.speaker",
        type: "link",
        icon: "important_devices" , isherf: true}] ,
    musthide: true
  } ,

  {
    state: "",
    name: "footer.social",
    icon: "home",
    type: "link" ,
    musthide: true ,
    children:[{
      name: "footer.facebook",
      state: "",
      icon: "home",
      type: "link"
    } , {
      name: "footer.twitter" ,
      state: "",
      icon: "home",
      type: "link"
    } , {
      name: "footer.google_plus",
      state: "",
      icon: "home",
      type: "link"
    } , {
      name: "footer.linked_in",
      state: "",
      icon: "home",
      type: "link"
    }]
  } ,
  {
    state: "",
    name: "footer.apps",
    icon: "home",
    type: "link" ,
    children:[{
      name: "footer.android",
      state: "",
      icon: "home",
      type: "link"
    } , {
      name: "footer.ios" ,
      state: "",
      icon: "home",
      type: "link"
    } ]
  }

];

const FooterMenusForMobile= [
  {
    state: "",
    name: "footer.your_account",
    icon: "home",
    type: "link" ,
    children:[{
      name: "footer.signup",
      state: "/register",
      icon: "home",
      type: "link"
    } , {
      name: "footer.login" ,
      state: "/login",
      icon: "home",
      type: "link"
    } , {
      name: "footer.forget_password",
      state: "/password/forget",
      icon: "home",
      type: "link"
    } , {
      name: "footer.help",
      state: "/contact",
      icon: "home",
      type: "link"
    }]
  }
  , {
    state: "",
    name: "footer.apps",
    icon: "home",
    type: "link" ,
    children:[{
      name: "footer.android",
      state: "",
      icon: "home",
      type: "link"
    } , {
      name: "footer.ios" ,
      state: "",
      icon: "home",
      type: "link"
    } ]
  }

];

@Injectable()
export class MenuItems {

   /*
    * Get all header menu
    */
   getMainMenu(): Menu[] {
      return HeaderOneItems;
   }

   /*
    * Get all footer menu
    */
   getFooterOneMenu(): Menu[] {
      return FooterMenus;
   }

   getFooterForMobile() :Menu[] {
     return FooterMenusForMobile;
   }
}
