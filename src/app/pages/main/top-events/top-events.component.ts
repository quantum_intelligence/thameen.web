import {Component, Input, OnInit} from '@angular/core';
import {EventModel} from '../../events/network/models/event.model';

@Component({
  selector: 'app-top-events',
  templateUrl: './top-events.component.html',
  styleUrls: ['./top-events.component.css']
})
export class TopEventsComponent implements OnInit{

  @Input() isRTL : boolean = false;

  slideConfig = {
    slidesToShow: 4,
    slidesToScroll:4,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll:1
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll:2
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll:1
        }
      }
    ]
  };
  events = new Array<EventModel>();

  constructor() {

  }

  ngOnInit() {
  }

}
