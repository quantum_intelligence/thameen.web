import { NgModule } from '@angular/core';
import { IndexComponent } from './index/index.component';
import {RouterModule} from '@angular/router';
import { SliderComponent } from './slider/slider.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {SharedModule} from '../../shared/shared.module';
import { TopEventsComponent } from './top-events/top-events.component';
import {CommonModule} from '@angular/common';
import {CommoEventModule} from '../common/commo.event.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import {FormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../app.module';
import {HttpClient} from '@angular/common/http';
import { TermsComponent } from './terms/terms.component';

export const routes = [
  { path: '', component: IndexComponent}, {path: 'top' , component: TopEventsComponent} , {path: 'contact' , component: ContactUsComponent} ,
  {path: 'about' , component: AboutUsComponent} , {path:'terms' , component: TermsComponent } , {path:'agreements' , component: TermsComponent },
  {path:'privacy' , component: TermsComponent } , {path:'disclaimer' , component: TermsComponent } , {path:'about' , component: TermsComponent }

];

@NgModule({
  declarations: [IndexComponent, SliderComponent , TopEventsComponent, AboutUsComponent, ContactUsComponent, TermsComponent],
  imports: [
    CommonModule ,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forChild(routes), SlickCarouselModule , SharedModule , CommoEventModule , FormsModule ,TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ]
})
export class MainModule { }
