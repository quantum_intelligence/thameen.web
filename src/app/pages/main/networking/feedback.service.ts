import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ContactUsModel} from './contact.us.model';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private httpclint: HttpClient) { }

  addFeedBack(feedback: ContactUsModel){
    return this.httpclint.post("https://api.thameen.site/api/contactus" , feedback);
  }
}
