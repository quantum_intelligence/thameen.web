import { Component, OnInit } from '@angular/core';
import {ContactUsModel} from '../networking/contact.us.model';
import {NgForm} from '@angular/forms';
import {FeedbackService} from '../networking/feedback.service';
import {Router} from '@angular/router';
import {AlertdialogService} from '../../../_helpers/alertdialog.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css'] ,
  providers: [FeedbackService]
})
export class ContactUsComponent implements OnInit {

  isLoading:boolean;
  contactusModel: ContactUsModel = new ContactUsModel();
  constructor(private feedbackSerivce: FeedbackService , private route:Router , private alertService:AlertdialogService) { }

  ngOnInit() {
  }

  onSubmit(form:NgForm){

   if(form.valid){
    this.alertService.openYesNoDialog("are you sure to send this message" , "Yes" , "No").afterClosed().subscribe(value => {
      if(value == 'yes'){
        this.feedbackSerivce.addFeedBack(this.contactusModel).subscribe(value => {
          this.alertService.openOkdialog("Thank  you!Your message has been sent successfully");
          this.route.navigate(['/']);
        } , error1 => {
          alert(error1.error.message);
        });
      }
    });
   }
  }

}
