import { Component, OnInit } from '@angular/core';
import {UserSettingService} from '../../../_helpers/user.setting.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  textdirc:string = "left";
  constructor(public  usersettingService:UserSettingService) {
    var lang = this.usersettingService.getLang();
    if(lang == 'ar'){
      this.textdirc = 'right';
    }else {
      this.textdirc = 'left';
    }
  }

  ngOnInit() {
  }

}
