import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {UserSettingService} from '../../../_helpers/user.setting.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit , OnChanges {

   isRTL : boolean = false;



  slideConfig : any;

  slides : any = [
    {
      img         : "assets/images/pic1.jpg",
      small_img: "assets/images/small_pic1.png",
      content     : "MOTAMARAT",
      heading_one : "ALL YOUR EVENTS IN ONE PLATFORMS",

    } ,
    {
      img         : "assets/images/pic2.jpg",
      small_img: "assets/images/small_pic2.png",
      content     : "MOTAMARAT",
      heading_one : "ALL YOUR EVENTS IN ONE PLATFORMS",

    }
    ,
    {
      img         : "assets/images/pic3.jpg",
      small_img: "assets/images/small_pic3.png",
      content     : "MOTAMARAT",
      heading_one : "ALL YOUR EVENTS IN ONE PLATFORMS",

    } ,
    {
      img         : "assets/images/pic4.jpg",
      small_img: "assets/images/small_pic4.png",
      content     : "MOTAMARAT",
      heading_one : "ALL YOUR EVENTS IN ONE PLATFORMS",

    }
  ];

  constructor(private usersettingService: UserSettingService) {
   var lang = this.usersettingService.getLang();
   if(lang == 'ar'){
     this.isRTL = true;
   }else {
     this.isRTL = false;
   }

    this.slideConfig = {
      slidesToShow: 1,
      slidesToScroll:1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      arrows: true,
      rtl: this.isRTL ,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll:1
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll:2
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll:1
          }
        }
      ]
    };
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.slideConfig = {
      slidesToShow: 1,
      slidesToScroll:1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      arrows: true,
      rtl : this.isRTL,
    responsive: [
        {
          breakpoint: 992,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll:1
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll:2
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll:1
          }
        }
      ]
    };
  }

}
