import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  termmodel: TermsModel = new TermsModel();
  constructor(private activerouter :ActivatedRoute) {
    this.activerouter.url.subscribe(value => {
      var storedvalue = value[0].path;
      if(storedvalue == "terms"){
        this.termmodel.header = "terms";
      }else if(storedvalue == "agreements"){
        this.termmodel.header = "agreements";
      }else if(storedvalue == "privacy"){
        this.termmodel.header = "privacy";
      }else if(storedvalue == "disclaimer"){
        this.termmodel.header = "disclaimer";
      }else if(storedvalue == "about"){
        this.termmodel.header = "about";
      }
      this.termmodel.body = "The Intellectual Property disclosure will inform users that the contents, logo and other visual media you created is your property and is protected by copyright laws.\n" +
        "A Termination clause will inform that users’ accounts on your website and mobile app or users’ access to your website and mobile (if users can’t have an account with you) can be terminated in case of abuses or at your sole discretion.\n" +
        "A Governing Law will inform users which laws govern the agreement. This should the country in which your company is headquartered or the country from which you operate your website and mobile app.\n" +
        "A Links To Other Web Sites clause will inform users that you are not responsible for any third party websites that you link to. This kind of clause will generally inform users that they are responsible for reading and agreeing (or disagreeing) with the Terms and Conditions or Privacy Policies of these third parties.\n" +
        "If your website or mobile app allows users to create content and make that content public to other users, a Content section will inform users that they own the rights to the content they have created. The “Content” clause usually mentions that users must give you (the website or mobile app developer) a license so that you can share this content on your website/mobile app and to make it available to other users.\n" +
        "\n" +
        "Because the content created by users is public to other users, a DMCA notice clause (or Copyright Infringement ) section is helpful to inform users and copyright authors that, if any content is found to be a copyright infringement, you will respond to any DMCA takedown notices received and you will take down the content." +
        "The Intellectual Property disclosure will inform users that the contents, logo and other visual media you created is your property and is protected by copyright laws.\n" +
        "A Termination clause will inform that users’ accounts on your website and mobile app or users’ access to your website and mobile (if users can’t have an account with you) can be terminated in case of abuses or at your sole discretion.\n" +
        "A Governing Law will inform users which laws govern the agreement. This should the country in which your company is headquartered or the country from which you operate your website and mobile app.\n" +
        "A Links To Other Web Sites clause will inform users that you are not responsible for any third party websites that you link to. This kind of clause will generally inform users that they are responsible for reading and agreeing (or disagreeing) with the Terms and Conditions or Privacy Policies of these third parties.\n" +
        "If your website or mobile app allows users to create content and make that content public to other users, a Content section will inform users that they own the rights to the content they have created. The “Content” clause usually mentions that users must give you (the website or mobile app developer) a license so that you can share this content on your website/mobile app and to make it available to other users.\n" +
        "\n"
    });
  }

  ngOnInit() {
  }

}


export class TermsModel {
  header:string;
  body:string;
}
