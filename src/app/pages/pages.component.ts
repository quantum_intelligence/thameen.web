import {APP_ID, Component, HostListener, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {interval} from 'rxjs';
import {NavigationEnd, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {Directionality} from '@angular/cdk/bidi';
import {MenuItems} from './home-stuff/menu/menu-items';
import {EmbryoService} from '../service/Embryo.service';
import {finalize, take, tap} from 'rxjs/operators';
import {UserSettingService} from '../_helpers/user.setting.service';
import {isPlatformBrowser} from '@angular/common';
import {UserProfileService} from '../_helpers/user.profile.service';
import {AccountModel} from './account/networks/account.model';

declare var $: any;
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  timer = 0;
  isRtl: any;
  currentUrl : any;
  accountmodel:AccountModel = new  AccountModel();
  imagepath:string = 'assets/images/profile_placeholder.png';

  constructor(public embryoService : EmbryoService,public menuItems: MenuItems,dir: Directionality,private router: Router,
              meta: Meta, title: Title ,public usersettingService: UserSettingService , public userProfileService:UserProfileService) {

    this.accountmodel = userProfileService.getProfile();
    title.setTitle('Motamarat');
      var lang = this.usersettingService.getLang();
       if(lang == 'ar'){
        this.isRtl = 'rtl';
       }
       else {
      this.isRtl = 'ltr';
    }

    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.currentUrl = event.url;
        }
      });
  }

  ngOnInit() {
    this.startTimer();
    if(this.accountmodel != null){
      if(this.accountmodel.imagePath !=null){
        this.imagepath = this.accountmodel.imagePath;
      }

    }

  }




  public startTimer() {
    this.timer = 0;
    interval(1000).pipe(
      take(3),
      tap(value => { this.timer = value + 1; }),
      finalize(() => {}),
    ).subscribe();

  }

  public hideSideNav() {
    this.embryoService.sidenavOpen = false;
  }

  public changeLang(){
    var lang = "ar";
    if(this.usersettingService.getLang() == 'ar') lang = 'en';
    this.usersettingService.changeLang(lang);
    window.location.reload();
  }

  /**
   * On window scroll add class header-fixed.
   */
  @HostListener('window:scroll', ['$event'])
  onScrollEvent($event){
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (scrollTop >= 200) {
      $("app-main").addClass("header-fixed");
    } else {
      $("app-main").removeClass("header-fixed");
    }
  }

}
