import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-payment-dialog',
  templateUrl: './payment-dialog.component.html',
  styleUrls: ['./payment-dialog.component.css']
})
export class PaymentDialogComponent implements OnInit {

  name = 'Angular 6';
  safeSrc: SafeResourceUrl;
  constructor(public dialogRef: MatDialogRef<PaymentDialogComponent> , @Inject(MAT_DIALOG_DATA) public passeddata: any , private sanitizer: DomSanitizer) {
    this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl("https://api-motamarat.kfshrc.edu.sa/payment/MoayserPayment?amount="+passeddata.amount+"&type="+passeddata.type+"&regId="+ passeddata.regid);
  }

  ngOnInit() {}

  close(){
    window.location.reload();
  }

}
