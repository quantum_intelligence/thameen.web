import { Component, OnInit } from '@angular/core';
import {EventsService} from '../network/services/events.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventDetailsModel} from '../network/models/event.details.model';
import {AlertdialogService} from '../../../_helpers/alertdialog.service';
import {ScrollService} from '../../../_helpers/scroll.service';
import {MatDialog} from '@angular/material';
import {PaymentDialogComponent} from '../payment-dialog/payment-dialog.component';
import {UserSettingService} from '../../../_helpers/user.setting.service';
declare let Paytabs: any;


@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  eventmodel: EventDetailsModel;
  isLoading: boolean;

  constructor(private eventService: EventsService , private activeroute: ActivatedRoute ,private router: Router,
              private alertSerivce: AlertdialogService, private scrollService:ScrollService , public dialog: MatDialog, private settingserivce:UserSettingService) {
    activeroute.params.subscribe(value => {
      this.isLoading = true;
    this.getEvent( value['id']);
    });

    this.scrollService.scrollTop();
  }

  ngOnInit() {
  }

  getEvent(eventid:any){
    this.eventService.getEventybyId(eventid).subscribe(value => {
      console.log(value);
      this.eventmodel = value;
      this.isLoading = false;


    });
  }

  getDateOnly(stringDate:string){
    var dateoftime = stringDate.split(' ')[0].split('/');
   if(this.settingserivce.getLang() == 'ar'){
     return dateoftime[0] + "/" + dateoftime[1] + "/" + dateoftime[2];
   }else{
     return dateoftime[0] + "/" + dateoftime[1] + "/20" + dateoftime[2];
   }

  }

  openRegistertion(){

    if(this.eventmodel.isHavePendingRegistration){

      if(this.eventmodel.paymentTypeId ==3){

        this.alertSerivce.openYesNoDialog("Do You Want To Confirm Your Event Subscription" , "Yes" , "No").afterClosed().subscribe(value => {
          if(value == 'yes'){
           this.paymentCall();
          }
        });

      }else {
        this.paymentCall();
      }

    }else{
      this.router.navigate(["/events/"+ this.eventmodel.id + "/registration"]);
    }


  }

  confirmPendingEvent(){
    this.eventService.payEvent(this.eventmodel.pendingRegistrationId ,{transactionNo : 'free' , transactionNote: 'free'}).subscribe(value1 => {
      this.alertSerivce.openOkdialog("Great! Now Your Subscription Has Been Confirmed");
    } , error1 => {
      alert(error1.error.message);
    });
  }

  paymentCall(){
    this.eventService.getEventRegistration(this.eventmodel.pendingRegistrationId).subscribe(value => {

      window.open("https://api-motamarat.kfshrc.edu.sa/payment/MoayserPayment?amount="+value.amount+"&type="+1+"&regId="+ this.eventmodel.pendingRegistrationId, '_blank');

      /*let dialogRef = this.dialog.open(PaymentDialogComponent , {
        width: '50%' ,
        data :{eventId: this.eventmodel.id , amount : value.amount , type : 1 , regid: this.eventmodel.pendingRegistrationId }
      });*/

    /*  dialogRef.afterClosed().subscribe(result => {
        //when you close the dilaog
      });*/

    } ,error1 => {
      alert(error1.error.message);
    });


  }

  getEventStatus(status: boolean){
    if(status){
      return 'events.pay_event';
    }
    return 'events.reg_event';
  }

}
