import {Component, Input, OnInit} from '@angular/core';
import {SessionModel} from '../network/models/session.model';

@Component({
  selector: 'app-session-card',
  templateUrl: './session-card.component.html',
  styleUrls: ['./session-card.component.css']
})
export class SessionCardComponent implements OnInit {
  @Input() session : SessionModel;
  @Input() index   : any;
  constructor() { }

  ngOnInit() {
  }

}
