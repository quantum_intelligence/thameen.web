import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SessionModel, Speaker} from '../network/models/session.model';
import {EventsService} from '../network/services/events.service';
import {MatDialog} from '@angular/material';
import {SessionMaterialDialogComponent} from '../session-material-dialog/session-material-dialog.component';
import {UserSettingService} from '../../../_helpers/user.setting.service';
import {Router} from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-sessions-list',
  templateUrl: './sessions-list.component.html',
  styleUrls: ['./sessions-list.component.css']
})
export class SessionsListComponent implements OnInit {


  sessions:SessionModel[];
  @Input()
  eventid: number;

  @Input()
  sessionsDate: string;

  textdirc:string = "left";
  placeimage:String = 'assets/images/profile_placeholder.png';

  constructor(private eventService: EventsService , public dialog: MatDialog , private userSettingService: UserSettingService , private router: Router , private settingserivce:UserSettingService) {
    var lang = this.userSettingService.getLang();
    if(lang == 'ar'){
      this.textdirc = 'right';
    }else {
      this.textdirc = 'left';
    }
  }

  ngOnInit() {
    this.loadSessionsbyDate();
  }

  loadSessionsbyDate(){
    this.eventService.getEventSessionsByDate(this.sessionsDate , this.eventid).subscribe(value => {
      this.sessions = value;
    });
  }

  getTimeOnly(stringDate:string){
    var dateoftime = stringDate.split(' ');
    return dateoftime[1] + " " + dateoftime[2];
  }

  getDateOnly(stringDate:string){
    var dateoftime = stringDate.split(' ')[0].split('/');
    if(this.settingserivce.getLang() == 'ar'){
      return dateoftime[0] + "/" + dateoftime[1] + "/" + dateoftime[2];
    }else{
      return dateoftime[0] + "/" + dateoftime[1] + "/20" + dateoftime[2];
    }

  }

  openMatrialDialog(sessionId:number){
    let dialogRef = this.dialog.open(SessionMaterialDialogComponent, {
      data: {sessionId: sessionId},
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      //when you close the dilaog
    });
  }



}
