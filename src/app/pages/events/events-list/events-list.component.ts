import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../network/models/product.model';
import {EventModel} from '../network/models/event.model';
import {EventsService} from '../network/services/events.service';
import {ActivatedRoute} from '@angular/router';
import {AccountService} from '../../account/networks/account.service';
import {ScrollService} from '../../../_helpers/scroll.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit {
  events = new Array<EventModel>();
  useEventcard = true;
  isLoading: boolean;
  isEmptydata:boolean;

  constructor(private eventService: EventsService , private activeRoute: ActivatedRoute , private accountSerivce:AccountService , private scrollService:ScrollService) {
    this.isLoading = true;
    this.isEmptydata = false;
    var type =  activeRoute.snapshot.data["type"];
    if(type == "my_event"){
      this.loadMyEvents();
      this.isEmptydata = true;
      console.log("it is my events");
    }else{
      this.loadEvents();
      console.log("it is list of events");
    }

    this.scrollService.scrollTop();
  }

  ngOnInit() {
  }

  changeDisplay(type){
    if(type == 'list'){
      this.useEventcard = false;

    }else{
      this.useEventcard = true;
    }
  }


  loadEvents(){
    this.eventService.getEvents().subscribe(value => {
      this.events = value;
      this.isLoading = false;

      if(value.length ==0){
        this.isEmptydata = true;
      }

    });
  }

  loadMyEvents(){
     this.accountSerivce.getMyProfile().subscribe(value => {
       this.eventService.getMyEvents(value.id).subscribe(value => {
         this.events = value;
         this.isEmptydata = false;
         this.isLoading = false;
         if(value.length ==0){
           this.isEmptydata = true;
         }
       });
     });

  }


}
