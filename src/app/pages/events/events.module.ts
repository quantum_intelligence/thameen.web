import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SponsorsComponent } from '../common/sponsors/sponsors.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import { EventDetailsComponent } from './event-details/event-details.component';
import { SessionsListComponent } from './sessions-list/sessions-list.component';
import {CommoEventModule} from '../common/commo.event.module';
import {EventsListComponent} from './events-list/events-list.component';
import { EventsFilterComponent } from '../common/events-filter/events-filter.component';
import { SpeakerProfileComponent } from './speaker-profile/speaker-profile.component';
import { SessionCardComponent } from './session-card/session-card.component';
import {FormsModule} from '@angular/forms';
import { EventMaterialComponent } from './event-material/event-material.component';
import {SessionMaterialDialogComponent} from './session-material-dialog/session-material-dialog.component';
import { EventRegistrationComponent } from './event-registration/event-registration.component';
import {LoginActiveGuard} from '../../_guards/login.active.guard';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../app.module';
import {HttpClient} from '@angular/common/http';
import { PaymentDialogComponent } from './payment-dialog/payment-dialog.component';


export const routes = [
  { path: 'sponsors', component: SponsorsComponent }
  , {path:'details/:id' , component: EventDetailsComponent}
  , {path:'' , component : EventsListComponent , data: {'type': 'event_list'}} ,
  {path: 'me' , component: EventsListComponent  , data: {'type': 'my_event'} , canActivate: [LoginActiveGuard]} ,
  {path: 'filter' , component: EventsFilterComponent} , {path : 'speakers/:id' , component: SpeakerProfileComponent}
  , {path: ':id/registration' , component: EventRegistrationComponent , canActivate: [LoginActiveGuard]}

];

@NgModule({
  declarations: [EventDetailsComponent, SessionsListComponent , EventsListComponent, SpeakerProfileComponent,
    SessionCardComponent, SessionMaterialDialogComponent, EventMaterialComponent, EventRegistrationComponent, PaymentDialogComponent],
  imports: [
    CommonModule ,RouterModule.forChild(routes), SlickCarouselModule , SharedModule , CommoEventModule , FormsModule  , TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ] ,
  entryComponents: [SessionMaterialDialogComponent , PaymentDialogComponent]
})
export class EventsModule { }
