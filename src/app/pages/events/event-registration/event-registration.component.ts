import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {EventRegistrationInputModel} from '../network/models/event.registration.input.model';
import {EventPriceModel} from '../network/models/event.price.model';
import {EventsService} from '../network/services/events.service';
import {SessionPriceForUserViewModel, SessionPricesModel} from '../network/models/session.prices.model';
import {AccountModel} from '../../account/networks/account.model';
import {UserProfileService} from '../../../_helpers/user.profile.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {AlertDialogComponent} from '../../../dialogs/registeration-alert/alert-dialog.component';
import {AlertdialogService} from '../../../_helpers/alertdialog.service';
import {PayforeventModel} from '../network/models/payforevent.model';
import {ScrollService} from '../../../_helpers/scroll.service';
import {PaymentDialogComponent} from '../payment-dialog/payment-dialog.component';
import {MobileConfirmationComponent} from '../../../anonymous/mobile-confirmation/mobile-confirmation.component';
declare let Paytabs: any;
@Component({
  selector: 'app-event-registration',
  templateUrl: './event-registration.component.html',
  styleUrls: ['./event-registration.component.css'] ,
  providers:[AlertdialogService]
})
export class EventRegistrationComponent implements OnInit {

  eventRegistrationInputModel:EventRegistrationInputModel = new EventRegistrationInputModel();
  eventPricesTypes: EventPriceModel[];
  sessionPrices : SessionPriceForUserViewModel[];
  selectedSessionPrices: number[] =[];
  eventId:number;
  total:number = 0;
  accountModel:AccountModel;
  sessionPricesModel : SessionPricesModel;
  filepath:string="assets/images/no-image.png";
  constructor(private eventSerivce: EventsService , private route: Router , activeRoute:ActivatedRoute , public accountService: UserProfileService ,
              private alertService:AlertdialogService , private scrollService:ScrollService , public dialog: MatDialog) {
  this.eventId = activeRoute.snapshot.params["id"];
  this.accountModel = this.accountService.getProfile();
  this.autofillData();
  this.scrollService.scrollTop();
  }

  handelPaymentType(){
    if(this.sessionPricesModel.paymentTypeId ==1 || this.sessionPricesModel.paymentTypeId == 2){
      this.total = this.sessionPricesModel.amount;
     /* for (let session of this.sessionPrices) {
        session.isSelected = true;
      }*/
    }
  }

  autofillData(){
    this.eventRegistrationInputModel.fullName = this.accountModel.fullName;
    this.eventRegistrationInputModel.jobTitle = this.accountModel.jobTitle;
    // this.eventRegistrationInputModel.profession = this.accountModel.profession;
    this.eventRegistrationInputModel.organizationName = this.accountModel.organizationName;
    this.eventRegistrationInputModel.arabicFullName = this.accountModel.arabicFullName;
  }

  ngOnInit() {
    this.getEventPriceTypes();
  }

  getEventPriceTypes(){
    this.eventSerivce.getEventPricesTypes(this.eventId).subscribe(value => {
      this.eventPricesTypes = value;
    });
  }

  getSessionPrices(){
    this.eventSerivce.getSessionPirces(this.eventId , this.eventRegistrationInputModel.priceTypeId).subscribe(value => {
      this.sessionPrices = value.sessionPriceForUserViewModels;
      this.sessionPricesModel = value;
      this.handelPaymentType();
    });
  }

  onPriceSelect(){
    this.total = 0;
    this.selectedSessionPrices = [];
    this.getSessionPrices();
  }

  onSesssionSelect(id:number){
    var SelectedsessionPrice = this.selectedSessionPrices.find(value => value == id);
    var sessionPrice = this.sessionPrices.find(value => value.id == id);

    if (SelectedsessionPrice) {

      if (SelectedsessionPrice == id) {

        var index = this.selectedSessionPrices.indexOf(SelectedsessionPrice);
        this.selectedSessionPrices.splice(index,1);
        this.total-= sessionPrice.amount;

      } else {

        this.selectedSessionPrices.push(sessionPrice.id);
        this.total+= sessionPrice.amount;
      }
    } else {
      var sessionPrice = this.sessionPrices.find(value => value.id == id);
      this.selectedSessionPrices.push(sessionPrice.id);
      this.total+= sessionPrice.amount;
    }
  }

  onSubmit(form:NgForm){
    if(form.valid){
      this.eventRegistrationInputModel.sessionsId = this.selectedSessionPrices;
      this.eventRegistrationInputModel.registerationTypeId =1;
      this.eventRegistrationInputModel.idCardImagePath = this.filepath;
      this.eventSerivce.registerOnEvent(this.eventId , this.eventRegistrationInputModel).subscribe(value => {
        this.openDialog(value.id);
      } , error1 => {
        if(error1.error.code =='mobile_confirmation'){
          this.alertService.openOkdialog('You Must Add Your Phone Number (With key like +966) And Save Profile To Receive Confirmation Code');
         this.route.navigate(['/account/profile']);
        }else{
          alert(error1.error.message);
        }
      });
    }else{
     this.alertService.openOkdialog("please add your identity");
    }
  }

  openDialog(regid){

    this.route.navigate(["/events/details/"+this.eventId ]);
    if(this.sessionPricesModel.paymentTypeId ==3){
      this.paymentCall(regid);
    }else {
      var message = 'Great You Registered On Event , Do You Want to Pay Now?';
      this.alertService.openYesNoDialog(message , "Yes Pay Now" , "No , Later").afterClosed().subscribe(value => {
        if(value == 'yes'){
          this.paymentCall(regid);
        }else{

        }
      })
    }

  }

  paymentCall(regId :number){
    window.open("https://api-motamarat.kfshrc.edu.sa/payment/MoayserPayment?amount="+this.total+"&type="+1+"&regId="+ regId, '_blank');
   /* let dialogRef = this.dialog.open(PaymentDialogComponent , {
      width: '50%' ,
      data :{eventId: this.eventId , amount : this.total , type : 1 , regid: regId}
    });

    dialogRef.afterClosed().subscribe(result => {
      //when you close the dilaog
    });*/
  }

  openMobileConfigrmation(){
    let dialogRef = this.dialog.open(MobileConfirmationComponent , {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      //when you close the dilaog
    });
  }


  onFileSelect(event: any) {
    var file = event.target.files[0];
    let formdata = new FormData();
    formdata.append('file', file, file.name);

    let headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options = {headers: headers};

    this.eventSerivce.uploadFile(formdata, options).subscribe(value => {
      // @ts-ignore
      this.filepath = value.filePath;
    }, error1 => {
      alert(error1.error.message);
    });


  }

}
