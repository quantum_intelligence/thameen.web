import {Component, Input, OnInit} from '@angular/core';
import {EventsService} from '../network/services/events.service';
import {EventMaterialModel} from '../network/models/event.material.model';

@Component({
  selector: 'app-event-material',
  templateUrl: './event-material.component.html',
  styleUrls: ['./event-material.component.css']
})
export class EventMaterialComponent implements OnInit {

  eventMaterials: EventMaterialModel[];

  @Input()
  eventid:number;

  constructor(private eventService: EventsService) { }

  ngOnInit() {
    this.loadEventMaterial();
  }

  loadEventMaterial(){
    this.eventService.getEventMaterial(this.eventid).subscribe(value => {
      this.eventMaterials = value;
    });
  }

}
