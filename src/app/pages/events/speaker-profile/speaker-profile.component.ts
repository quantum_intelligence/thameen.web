import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EventsService} from '../network/services/events.service';
import {UserProfileModel} from '../network/models/user.profile.model';
import {ScrollService} from '../../../_helpers/scroll.service';

@Component({
  selector: 'app-speaker-profile',
  templateUrl: './speaker-profile.component.html',
  styleUrls: ['./speaker-profile.component.css']
})
export class SpeakerProfileComponent implements OnInit {

  speaker : UserProfileModel = new UserProfileModel();
  userProfile: string;
  isLoading:boolean;

  constructor(private eventSerivce: EventsService , private activeroute:ActivatedRoute , private scrollService:ScrollService) {
    this.userProfile = activeroute.snapshot.params["id"];
    this.isLoading = true;
    this.loadUserData();
    this.scrollService.scrollTop();
  }

  ngOnInit() {
  }

  loadUserData(){
  this.eventSerivce.getUserProfile(this.userProfile).subscribe(value => {
    this.speaker = value;
    this.isLoading = false;
  });

  }

}
