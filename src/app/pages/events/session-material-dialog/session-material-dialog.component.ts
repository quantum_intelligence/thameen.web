import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SessionMaterialModel} from '../network/models/session.material.model';
import {EventsService} from '../network/services/events.service';

@Component({
  selector: 'app-event-material-dialog',
  templateUrl: './session-material-dialog.component.html',
  styleUrls: ['./session-material-dialog.component.css']
})
export class SessionMaterialDialogComponent implements OnInit {

  sessionMaterials: SessionMaterialModel[];
  isLoading:boolean;

  constructor(public dialogRef: MatDialogRef<SessionMaterialDialogComponent> , @Inject(MAT_DIALOG_DATA) public passeddata: any , private eventService: EventsService) { }

  ngOnInit() {
    this.isLoading = true;
    this.loadSessionMaterial();
  }

  loadSessionMaterial(){
   this.eventService.getSessionMaterial( this.passeddata.sessionId).subscribe(value => {
     this.sessionMaterials = value;
     this.isLoading = false;
   });
  }

}
