import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EventModel} from '../models/event.model';
import {EventDetailsModel} from '../models/event.details.model';
import {SessionModel} from '../models/session.model';
import {EventSponsorModel} from '../models/event.sponsor.model';
import {EventTypeModel} from '../models/event.type.model';
import {EventSpecialityModel} from '../models/event.speciality.model';
import {EventMaterialModel} from '../models/event.material.model';
import {SessionMaterialModel} from '../models/session.material.model';
import {EventPriceModel} from '../models/event.price.model';
import {EventRegistrationInputModel} from '../models/event.registration.input.model';
import {SessionPricesModel} from '../models/session.prices.model';
import {UserProfileModel} from '../models/user.profile.model';
import {PayforeventModel} from '../models/payforevent.model';
import {FileModel} from '../../../account/networks/file.model';
import {updateBinding} from '@angular/core/src/render3/bindings';
import {EventRegstrationModel} from '../models/event.regstration.model';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private httpclint: HttpClient) { }

  getEvents(){
    return this.httpclint.get<EventModel[]>("https://api.thameen.site/api/events?page=1");
  }

  getEventybyId(id: number){
    return this.httpclint.get<EventDetailsModel>("https://api.thameen.site/api/events/" +id);
  }

  getEventSessionsByDate(date:string , eventid:number){
    return this.httpclint.get<SessionModel[]>("https://api.thameen.site/api/sessions/events/"+eventid+"/time?date=" + date);
  }

  getEventSponsor(eventid:number){
    return this.httpclint.get<EventSponsorModel[]>("https://api.thameen.site/api/events/"+eventid+"/sponsors?page=1");
  }


  getEventsType(){
    return this.httpclint.get<EventTypeModel[]>("https://api.thameen.site/api/lookups/eventtypes");
  }

  getEventsSpecialities(){
    return this.httpclint.get<EventSpecialityModel[]>("https://api.thameen.site/api/lookups/specialties");
  }

  getEventByFilter(fromdate: string , todate:string , speciality:number , eventType: number){
    var filters = "";


     if(speciality){
      filters += "&specialtyIds="+speciality;
    }
     if(eventType){
      filters += "&eventtypeIds="+eventType;
    }
     if(fromdate){
      filters += "&fromdate="+fromdate;
    }

     if(todate){
      filters += "&todate="+todate;
    }

    return  this.httpclint.get<EventModel[]>("https://api.thameen.site/api/events?page=1" + filters);
  }

  getEventMaterial(eventId:number){
    return this.httpclint.get<EventMaterialModel[]>("https://api.thameen.site/api/events/"+eventId+"/materials");
  }

  getSessionMaterial(sessionId:number){
    return this.httpclint.get<SessionMaterialModel[]>("https://api.thameen.site/api/sessions/"+sessionId+"/files?page=1")
  }

  getEventPricesTypes(eventid:number){
    return this.httpclint.get<EventPriceModel[]>("https://api.thameen.site/api/events/"+eventid+"/prices");
  }

  registerOnEvent(eventId:number , eventregisterationmodel: EventRegistrationInputModel){
    return this.httpclint.post<EventRegstrationModel>("https://api.thameen.site/api/events/"+eventId+"/registrations" , eventregisterationmodel);
  }

  getSessionPirces(eventid:number , pricetype:number){
    return this.httpclint.get<SessionPricesModel>("https://api.thameen.site/api/events/"+eventid+"/sessions/price?priceTypeId="+ pricetype);
  }

  getUserProfile(userid: string){
    return this.httpclint.get<UserProfileModel>("https://api.thameen.site/api/users?userId=" + userid);
  }

  getMyEvents(userid:string){
    return this.httpclint.get<EventModel[]>("https://api.thameen.site/api/events/registrations/users/"+ userid);
  }

  payEvent(regId:string , payforevent:PayforeventModel){
    return this.httpclint.put("https://api.thameen.site/api/events/registrations/"+regId+"/pay" , payforevent);
  }

  public uploadFile(formdata: FormData , options:any){
    return this.httpclint.post<FileModel>('https://api.thameen.site/api/Uploads?filetype=1' , formdata , options);
  }

  public getEventRegistration(regid:string){
    return this.httpclint.get<EventRegstrationModel>("https://api.thameen.site/api/events/registrations/"+regid);
  }
//payment dialog contain urls

}
