export class ProductModel {
  name: string;
  image: string;
  price: string;
  type: string;
  id: string;
  currency: string;
  description: string;
}
