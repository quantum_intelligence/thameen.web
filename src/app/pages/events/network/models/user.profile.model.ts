export class UserProfileModel {

  id: string;
  fullName: string;
  email: string;
  userName: string;
  genderName: string;
  genderId: number;
  imagePath: string;
  coverImagePath: string;
  about: string;
  createdOnString: string;
  followersCount: number;
  followingCount: number;
  eventCount: number;
  contributionCount: number;
  organizationName?: any;
  profession?: any;
  jobTitle?: any;
  arabicFullName?: any;
  isCurrentUserFollowThisUser: boolean;
  isSpeaker: boolean;
  speakerRate: number;
  speakerId: number;
}
