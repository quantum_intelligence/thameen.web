export interface User {
  id: string;
  fullName: string;
  email: string;
  userName: string;
  genderName: string;
  genderId: number;
  imagePath: string;
  about: string;
  createdOnString: string;
  followersCount: number;
  followingCount: number;
  contributionCount: number;
  organizationName?: any;
  profession?: any;
  jobTitle?: any;
  arabicFullName?: any;
}

export interface Speaker {
  id: number;
  userId: string;
  resume: string;
  description: string;
  rate: number;
  user: User;
}

export interface Hall {
  id: number;
  capacity: number;
  arabicName: string;
  englishName: string;
  name: string;
  arabicDescription: string;
  englishDescription: string;
  description: string;
}

export interface SessionModel {
  id: number;
  eventId: number;
  speakerId: number;
  hallId: number;
  title: string;
  arabicTitle: string;
  englishTitle: string;
  description: string;
  arabicDescription: string;
  englishDescription: string;
  startOn: string;
  endOn: string;
  startOnString: string;
  endOnString: string;
  createdOn: string;
  createdOnString: string;
  speaker: Speaker;
  hall: Hall;
}
