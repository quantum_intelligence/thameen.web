export class SessionPriceForUserViewModel {
  id: number;
  title: string;
  arabicTitle: string;
  englishTitle: string;
  description: string;
  arabicDescription: string;
  englishDescription: string;
  startOn: Date;
  endOn: Date;
  startOnString: string;
  endOnString: string;
  isEarlyPrice: boolean;
  amount: number;
  isSelected:boolean = false;
}

export interface SessionPricesModel {
  id: number;
  paymentTypeId: number;
  name: string;
  arabicName: string;
  englishName: string;
  amount: number;
  sessionPriceForUserViewModels: SessionPriceForUserViewModel[];
}
