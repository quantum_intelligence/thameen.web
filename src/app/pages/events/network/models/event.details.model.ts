export interface Specialty {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}

export interface EventType {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}

export interface Center {
  id: number;
  name: string;
  address: string;
  arabicName: string;
  englishName: string;
  arabicAddress: string;
  englishAddress: string;
  locationImage: string;
  latitude: number;
  longitude: number;
}

export interface PaymentType {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}

export interface User {
  id: string;
  fullName: string;
  email: string;
  userName: string;
  genderName: string;
  genderId: number;
  imagePath: string;
  about: string;
  createdOnString: string;
  followersCount: number;
  followingCount: number;
  contributionCount: number;
  organizationName?: any;
  profession?: any;
  jobTitle?: any;
  arabicFullName?: any;
}

export interface Speaker {
  id: number;
  userId: string;
  resume: string;
  description: string;
  rate: number;
  user: User;
}

export interface EventDay {
  name: string;
  time: string;
  sessionCounts: number;
  eventId: number;
}

export interface Material {
  id: number;
  eventId: number;
  filePath: string;
  totalViews: number;
  arabicName: string;
  englishName: string;
  arabicDescription: string;
  englishDescription: string;
  name: string;
  description: string;
  createdOn: Date;
  createdOnString: string;
}

export interface AttendanceType {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}


export interface EventDetailsModel {
  id: number;
  specialtyId: number;
  eventTypeId: number;
  centerId: number;
  paymentTypeId: number;
  name: string;
  arabicName: string;
  englishName: string;
  notes: string;
  arabicNotes: string;
  englishNotes: string;
  hours: number;
  hoursDescription: string;
  hoursArabicDescription: string;
  hoursEnglishDescription: string;
  body: string;
  arabicBody: string;
  englishBody: string;
  eventImage: string;
  createdOn: string;
  createdOnString: string;
  eventOn: string;
  eventOnString: string;
  eventEndOn: string;
  eventEndOnString: string;
  registerationOpenOn: string;
  registerationOpenOnString: string;
  registerationEndOn: string;
  registerationEndOnString: string;
  earlyRegisterationEndOn: string;
  earlyRegisterationEndOnString: string;
  eventDaysDuration: number;
  eventPrices?: any;
  specialty: Specialty;
  eventType: EventType;
  center: Center;
  paymentType: PaymentType;
  attendanceType: AttendanceType;
  subscribedUsersCount: number;
  subscribedUsers: any[];
  speakers: Speaker[];
  eventDays: EventDay[];
  materials: Material[];
  sponsors: any[];
  isRegisterationOpend: boolean;
  isEarlyRegisterationOpened: boolean;
  isCurrentUserRegistered: boolean;
  isCurrentUserFavorite: boolean;
  isHavePendingRegistration:boolean;
  pendingRegistrationId:string;

}
