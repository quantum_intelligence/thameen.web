export interface EventPriceModel {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}
