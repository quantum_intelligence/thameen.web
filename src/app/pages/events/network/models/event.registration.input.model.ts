export class EventRegistrationInputModel {
  registerationTypeId: number;
  priceTypeId: number;
  fullName: string;
  arabicFullName: string;
  organizationName: string;
  profession: string;
  jobTitle: string;
  idCardImagePath:string;
  sessionsId: number[];
}
