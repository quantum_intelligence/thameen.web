export interface SessionMaterialModel {
  id: number;
  sessionId: number;
  filePath: string;
  fileSizeBytes: number;
  fileSizeBytesString: string;
  createdOn: Date;
  createdOnString: string;
  totalViews: number;
}
