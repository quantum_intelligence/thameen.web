export interface EventPrice {
  id: number;
  eventId: number;
  priceTypeId: number;
  amount: number;
  isEarlyPrice: boolean;
  arabicName: string;
  englishName: string;
  name: string;
  isActive: boolean;
}

export interface Specialty {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}

export interface EventType {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}

export interface Center {
  id: number;
  name: string;
  address: string;
  arabicName: string;
  englishName: string;
  arabicAddress: string;
  englishAddress: string;
  locationImage: string;
  latitude: number;
  longitude: number;
}

export interface PaymentType {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}

export interface Eventdays {
  name: string;
  time: string;
  sessionCounts: number;
  eventId: number;
}

export interface EventModel {
  id: number;
  specialtyId: number;
  eventTypeId: number;
  centerId: number;
  paymentTypeId: number;
  name: string;
  arabicName: string;
  englishName: string;
  notes: string;
  arabicNotes: string;
  englishNotes: string;
  hours: number;
  hoursDescription: string;
  hoursArabicDescription: string;
  hoursEnglishDescription: string;
  body: string;
  arabicBody: string;
  englishBody: string;
  eventImage: string;
  createdOn: string;
  createdOnString: string;
  eventOn: string;
  eventOnString: string;
  eventEndOn: string;
  eventEndOnString: string;
  registerationOpenOn: string;
  registerationOpenOnString: string;
  registerationEndOn: string;
  registerationEndOnString: string;
  earlyRegisterationEndOn: string;
  earlyRegisterationEndOnString: string;
  eventDaysDuration: number;
  eventPrices: EventPrice[];
  specialty: Specialty;
  eventType: EventType;
  center: Center;
  paymentType: PaymentType;
  subscribedUsersCount: number;
  subscribedUsers?: any;
  speakers?: any;
  eventDays?: Eventdays[];
  materials?: any;
  sponsors?: any;
  isRegisterationOpend: boolean;
  isEarlyRegisterationOpened: boolean;
}
