export interface EventTypeModel {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}
