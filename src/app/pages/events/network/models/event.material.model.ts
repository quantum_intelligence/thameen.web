export interface EventMaterialModel {
  id: number;
  eventId: number;
  filePath: string;
  totalViews: number;
  arabicName: string;
  englishName: string;
  arabicDescription: string;
  englishDescription: string;
  name: string;
  description: string;
  createdOn: Date;
  createdOnString: string;
}
