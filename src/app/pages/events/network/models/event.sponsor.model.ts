export interface EventSponsorModel {
  id: number;
  name: string;
  arabicName: string;
  englishName: string;
  logo: string;
  description: string;
  arabicDescription: string;
  englishDescription: string;
}
