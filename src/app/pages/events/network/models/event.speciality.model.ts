export interface EventSpecialityModel {
  id: number;
  arabicName: string;
  englishName: string;
  name: string;
}
