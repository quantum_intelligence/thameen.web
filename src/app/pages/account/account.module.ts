import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UpdateComponent} from './update/update.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';


export const routes = [{path: 'profile' , component: UpdateComponent}];
@NgModule({
  declarations: [UpdateComponent],
  imports: [
    CommonModule , RouterModule.forChild(routes) , SharedModule , FormsModule
  ]
})
export class AccountModule { }
