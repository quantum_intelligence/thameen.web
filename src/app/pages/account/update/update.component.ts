import { Component, OnInit } from '@angular/core';
import {AccountService} from "../networks/account.service";
import {AccountModel} from "../networks/account.model";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {UserProfileService} from '../../../_helpers/user.profile.service';
import {MatDialog} from '@angular/material';
import {MobileConfirmationComponent} from '../../../anonymous/mobile-confirmation/mobile-confirmation.component';
import {EventSpecialityModel} from '../../events/network/models/event.speciality.model';
import {EventsService} from '../../events/network/services/events.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'] ,
  providers:[AccountService]
})
export class UpdateComponent implements OnInit {

 public accountModel: AccountModel = new AccountModel();
  isLoading:boolean;
  imagePath:string = "assets/images/profile_placeholder.png";
  specilities: EventSpecialityModel[] = new Array<EventSpecialityModel>();

  constructor(private accountService: AccountService , private route: Router , private userProfile:UserProfileService
              , public dialog: MatDialog , private eventService:EventsService) {
    this.isLoading = true;
    this.loadSepcaility();
   this.loadProfile();
  }

  ngOnInit() {
  }

  loadProfile(){
    this.accountService.getMyProfile().subscribe(value => {
      this.accountModel = value;
      this.isLoading = false;
      if(this.accountModel.imagePath){
        this.imagePath = this.accountModel.imagePath;
      }
    });
  }

  onSubmit(form:NgForm){
    if(form.valid){
      this.accountModel.imagePath = this.imagePath;
     this.accountService.updateProfile(this.accountModel).subscribe(value => {
       this.userProfile.saveProfile(this.accountModel);
       alert('your profile has been updated');
       location.reload();
     } , error1 => {
       if(error1.error.code == "mobile_confirmation"){
         this.userProfile.saveProfile(this.accountModel);
         this.openMobileConfigrmation();
       }
     });
    }
  }

  openMobileConfigrmation(){
    let dialogRef = this.dialog.open(MobileConfirmationComponent , {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
    location.reload();
    });
  }

  onFileSelect(event: any){
    var file = event.target.files[0];
    let formdata = new FormData();
    formdata.append('file' , file , file.name);

    let headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options = { headers: headers };

    this.accountService.uploadFile(formdata , options).subscribe(value => {
      // @ts-ignore
      this.imagePath = value.filePath;
      // @ts-ignore
      this.accountModel.imagePath = value.filePath;
    } , error1 => {

    });

  }

  loadSepcaility(){
    this.eventService.getEventsSpecialities().subscribe(value => {
      this.specilities = value;
    })
  }

}
