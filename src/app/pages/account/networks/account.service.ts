import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AccountModel} from "./account.model";
import {RegistrationModel} from '../../../anonymous/networks/registration.model';
import {FileModel} from './file.model';
import {UserProfileModel} from '../../events/network/models/user.profile.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpclint: HttpClient) { }


  getMyProfile(){
    return this.httpclint.get<AccountModel>('https://api.thameen.site/api/accounts');
  }

  updateProfile(accountmodel:AccountModel){
    return this.httpclint.put<AccountModel>('https://api.thameen.site/api/accounts' , accountmodel);
  }

  registerUser(registermodel: RegistrationModel){
    return this.httpclint.post<UserProfileModel>('https://api.thameen.site/api/accounts/register' , registermodel);
  }

  public uploadFile(formdata: FormData , options:any){

    return this.httpclint.post<FileModel>('https://api.thameen.site/api/Uploads?filetype=1' , formdata , options);
  }

}
