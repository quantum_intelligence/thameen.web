import {Component, Inject, OnInit} from '@angular/core';
import {CustomAuthService} from '../networks/custom.auth.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-mobile-confirmation',
  templateUrl: './mobile-confirmation.component.html',
  styleUrls: ['./mobile-confirmation.component.css']
})
export class MobileConfirmationComponent implements OnInit {

  code:string= "";
  isLoading:boolean = false;

  constructor(private authservice:CustomAuthService , public dialogRef: MatDialogRef<MobileConfirmationComponent> , @Inject(MAT_DIALOG_DATA) public passeddata: any) {
  }

  ngOnInit() {
  }

  sendClick(code:string){
    this.isLoading = true;
    this.authservice.mobileConfiramation(code).subscribe(value => {
      this.dialogRef.close();
    } , error1 => {
      this.isLoading = false;
      alert(error1.error.message);
    })
  }

}
