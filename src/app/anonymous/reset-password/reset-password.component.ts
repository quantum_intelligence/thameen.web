import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';
import {ForgetPasswordModel} from '../networks/forget.password.model';
import {CustomAuthService} from '../networks/custom.auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  forgetpassword: ForgetPasswordModel = new ForgetPasswordModel();

  constructor(private authservice: CustomAuthService , private router:Router , private activerouter:ActivatedRoute) {
    this.activerouter.params.subscribe(params => {
      this.forgetpassword.email  = params['email'];
    });
  }

  ngOnInit() {
  }

  onSubmit(form:NgForm){
    if(form.valid){
      this.authservice.resetPassowrd(this.forgetpassword).subscribe(value => {
        this.router.navigate(['/login']);
      } , error1 => {
        alert(error1.error.message);
      });
    }
  }

}
