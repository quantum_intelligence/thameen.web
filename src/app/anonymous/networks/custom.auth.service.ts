import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginModel} from './login.model';
import {TokenModel} from './token.model';
import {Observable} from 'rxjs';
import {EmailConfrmationModel} from './email.confrmation';
import {ForgetPasswordModel} from './forget.password.model';
import {SocailMediaModel} from './socail.media.model';

@Injectable({
  providedIn: 'root'
})
export class CustomAuthService {

  constructor(private httpclint: HttpClient) { }

  public login(loginmodel: LoginModel): Observable<TokenModel>{
    return this.httpclint.post<TokenModel>('https://api-motamarat.kfshrc.edu.sa/api/accounts/login' , loginmodel);
  }

  public socialLogin(socialmodel:SocailMediaModel){
    return this.httpclint.post<TokenModel>("https://api-motamarat.kfshrc.edu.sa/api/externalauth" , socialmodel);
  }

  public logout(){
    localStorage.removeItem('access_token');
    localStorage.removeItem("profile");
    location.reload(true);
  }

  public getUserToken(){
    return localStorage.getItem('access_token');
  }

  public islogedIn(){
    if(localStorage.getItem('access_token')){
      return true;
    }
    return false;
  }

  public emailConfirmation(emailconfirmation: EmailConfrmationModel) {
    return this.httpclint.post<TokenModel>('https://api-motamarat.kfshrc.edu.sa/api/accounts/confiramtion_email' , emailconfirmation);
  }
  public forgetPassword(model: ForgetPasswordModel){
    return this.httpclint.post('https://api-motamarat.kfshrc.edu.sa/api/accounts/forget' , model);
  }

  public resetPassowrd(model: ForgetPasswordModel){
    return this.httpclint.post('https://api-motamarat.kfshrc.edu.sa/api/accounts/confiramtion_password' , model);
  }

  public mobileConfiramation(code:string){
    return this.httpclint.post('https://api-motamarat.kfshrc.edu.sa/api/accounts/confiramtion_mobile' , {code: code});
  }

}
