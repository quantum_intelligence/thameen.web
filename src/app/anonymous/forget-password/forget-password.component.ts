import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router} from '@angular/router';
import {ForgetPasswordModel} from '../networks/forget.password.model';
import {CustomAuthService} from '../networks/custom.auth.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  forgetpassword: ForgetPasswordModel = new ForgetPasswordModel();
  constructor(private authserivce:CustomAuthService , private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form:NgForm){
    if(form.valid){
      this.authserivce.forgetPassword(this.forgetpassword).subscribe(value => {
        this.router.navigate(['password/reset' , {email: this.forgetpassword.email}])
      } , error1 => {
        alert(error1.error.message);
      })
    }
  }

}
