import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';
import {CustomAuthService} from '../networks/custom.auth.service';
import {EmailConfrmationModel} from '../networks/email.confrmation';
import {AccountService} from '../../pages/account/networks/account.service';
import {UserProfileService} from '../../_helpers/user.profile.service';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.css']
})
export class EmailConfirmationComponent implements OnInit {

  emailconfrmatin: EmailConfrmationModel = new EmailConfrmationModel();
  email: string;
  constructor(private authservice: CustomAuthService , route : ActivatedRoute , private router:Router ,
              private accountservice:AccountService , private profileService:UserProfileService) {
    route.params.subscribe(params => {
     this.email  = params['email'];
    });
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm){
    if(form.valid){
      this.emailconfrmatin.email = this.email;
      this.authservice.emailConfirmation(this.emailconfrmatin).subscribe(value => {
        localStorage.setItem('access_token' , value.tokenString);

        this.accountservice.getMyProfile().subscribe(value => {
          this.profileService.saveProfile(value);
          location.reload();
          this.router.navigate(['/']);
        });

       this.router.navigate(['/']);

      } , error1 => {
        alert(error1.error.message);
      });
    }
  }

}
