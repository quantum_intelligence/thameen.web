import { Component, OnInit } from '@angular/core';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular5-social-login';
import {LoginModel} from '../networks/login.model';
import {Router} from '@angular/router';
import {CustomAuthService} from '../networks/custom.auth.service';
import {NgForm} from '@angular/forms';
import {UserProfileService} from '../../_helpers/user.profile.service';
import {AccountService} from '../../pages/account/networks/account.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AccountService]
})
export class LoginComponent implements OnInit {

  loginmodel: LoginModel = new LoginModel();
  constructor(private socialAuthService: AuthService , private authservice : CustomAuthService, private router:Router ,
              private profileService:UserProfileService ,private accountservice:AccountService) { }

  ngOnInit() {
  }

  socialLogin(provider:string){
    var socialProvider = "";
    if(provider=='google'){
      socialProvider = GoogleLoginProvider.PROVIDER_ID;
    }else if(provider == 'facebook'){
      socialProvider = FacebookLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialProvider).then(value => {
      var loginModel;
         if(provider == "facebook"){
           loginModel = {accessToken: value.token , accessTokenSecret: value.token , provider: value.provider};
         }else {
          loginModel = {accessToken: value.idToken , accessTokenSecret: value.token , provider: value.provider}
         }

      this.authservice.socialLogin(loginModel).subscribe(res => {
        console.log(res);
        if(res.tokenString){
          localStorage.setItem('access_token' , res.tokenString);
          this.accountservice.getMyProfile().subscribe(value => {
            this.profileService.saveProfile(value);
            this.router.navigate(['/']);
          });

        }
      } , error1 => {
        alert(error1)
      });
    })
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.authservice.login(this.loginmodel).subscribe(res=> {
        if(res.tokenString){
          localStorage.setItem('access_token' , res.tokenString);
          this.accountservice.getMyProfile().subscribe(value => {
            this.profileService.saveProfile(value);
            this.router.navigate(['/']);
          });

        }
      } , error1 => {
        if(error1.error.code == 'email_confirmation'){
       this.router.navigate(['/email/confirmation' , {email: this.loginmodel.email}]);
        }else if(error1.error.code == 'credentials'){
          alert('password or email is incorrect');
        }
      })
    }
  }


}
