import { Component, OnInit } from '@angular/core';
import {RegistrationModel} from '../networks/registration.model';
import {AccountService} from '../../pages/account/networks/account.service';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registermodel : RegistrationModel = new RegistrationModel();
  isLoading:boolean;

  constructor(private accountservice: AccountService , private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form:NgForm) {
    if(form.valid){
      if(this.registermodel.password == this.registermodel.passowrdconfirmation){
        this.registerUser();
      }else{
        alert('Password is not match');
      }
    }

  }

  registerUser(){
    this.isLoading = true;
    this.accountservice.registerUser(this.registermodel).subscribe(value => {
      this.isLoading = false;
      this.router.navigate(['/email/confirmation' , {email: value.email}]);
    } , error1 => {
      this.isLoading = false;
      alert(error1.error.message);
    })
  }

}
