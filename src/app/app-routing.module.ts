import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PagesComponent} from './pages/pages.component';
import {LoginComponent} from './anonymous/login/login.component';
import {RegistrationComponent} from './anonymous/registration/registration.component';
import {ResetPasswordComponent} from './anonymous/reset-password/reset-password.component';
import {EmailConfirmationComponent} from './anonymous/email-confirmation/email-confirmation.component';
import {ForgetPasswordComponent} from './anonymous/forget-password/forget-password.component';
import {LoginActiveGuard} from './_guards/login.active.guard';


const routes: Routes = [
  {path: '' , component: PagesComponent  ,
    children: [{path: '' , loadChildren: './pages/main/main.module#MainModule'} ,
      {path: 'events' , loadChildren: './pages/events/events.module#EventsModule'} ,
      {path: 'account' , loadChildren: './pages/account/account.module#AccountModule' , canActivate:[LoginActiveGuard]}]} , {path: 'login' , component: LoginComponent}
      , {path: 'register' , component: RegistrationComponent} , {path: 'email/confirmation' , component: EmailConfirmationComponent}
      , {path: 'password/forget' , component: ForgetPasswordComponent} ,
  {path: 'password/reset' , component: ResetPasswordComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
