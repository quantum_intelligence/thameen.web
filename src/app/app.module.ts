import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {PagesComponent} from './pages/pages.component';
import {SharedModule} from './shared/shared.module';
import {HeaderComponent} from './pages/home-stuff/header/header.component';
import {EmbryoService} from './service/Embryo.service';
import { SideBarComponent } from './pages/home-stuff/side-bar/side-bar.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {MenuItems} from './pages/home-stuff/menu/menu-items';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FixHeaderComponent } from './pages/home-stuff/fix-header/fix-header.component';
import {MenuComponent} from './pages/home-stuff/menu/menu.component';
import {CommonModule} from '@angular/common';
import {AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule} from 'angular5-social-login';
import {MustMatchDirective} from './helpers/must.match.directive';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import {CustomAuthService} from './anonymous/networks/custom.auth.service';
import {LoginComponent} from './anonymous/login/login.component';
import {RegistrationComponent} from './anonymous/registration/registration.component';
import {FormsModule} from '@angular/forms';
import {EmailConfirmationComponent} from './anonymous/email-confirmation/email-confirmation.component';
import {ForgetPasswordComponent} from './anonymous/forget-password/forget-password.component';
import {ResetPasswordComponent} from './anonymous/reset-password/reset-password.component';
import { FooterComponent } from './pages/home-stuff/footer/footer.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {UserSettingService} from './_helpers/user.setting.service';
import {UserProfileService} from './_helpers/user.profile.service';
import {AlertDialogComponent} from './dialogs/registeration-alert/alert-dialog.component';
import {ScrollService} from './_helpers/scroll.service';
import { MobileConfirmationComponent } from './anonymous/mobile-confirmation/mobile-confirmation.component';



// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("352822878635822")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("687991120656-c67ua7514rqr1e503v2kstd6qh3i7qf9.apps.googleusercontent.com")
      },
    ]
)
  return config;
}

@NgModule({
  declarations: [
    AppComponent, PagesComponent ,HeaderComponent, SideBarComponent, FixHeaderComponent ,
    MenuComponent,MustMatchDirective , LoginComponent , RegistrationComponent ,
    EmailConfirmationComponent , ForgetPasswordComponent, ResetPasswordComponent, FooterComponent, AlertDialogComponent, MobileConfirmationComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'embryo-seo-pre'}) , AppRoutingModule , SharedModule,
    HttpClientModule , BrowserAnimationsModule ,CommonModule , SocialLoginModule , FormsModule  ,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [EmbryoService , MenuItems , {provide: AuthServiceConfig,useFactory: getAuthServiceConfigs} ,
    {provide:HTTP_INTERCEPTORS , useClass:ErrorInterceptor , multi: true}
    , {provide: HTTP_INTERCEPTORS , useClass:JwtInterceptor , multi: true } , CustomAuthService , UserSettingService , UserProfileService, ScrollService],
  bootstrap: [AppComponent] ,
  entryComponents: [AlertDialogComponent , MobileConfirmationComponent]
})
export class AppModule { }


// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
