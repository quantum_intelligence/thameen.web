import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {AlertDialogComponent} from '../dialogs/registeration-alert/alert-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class AlertdialogService {

  constructor(private dialog: MatDialog) { }

 public openYesNoDialog(message:string , yes:string , no: string):MatDialogRef<AlertDialogComponent>{
    let confirmationDialog : MatDialogRef<AlertDialogComponent>;
   confirmationDialog = this.dialog.open(AlertDialogComponent);
   confirmationDialog.componentInstance.message = message;
   confirmationDialog.componentInstance.yeslable = yes;
   confirmationDialog.componentInstance.nolable = no;
   return confirmationDialog;
  }

  public openOkdialog(message:string) :MatDialogRef<AlertDialogComponent> {
    let confirmationDialog : MatDialogRef<AlertDialogComponent>;
    confirmationDialog = this.dialog.open(AlertDialogComponent);
    confirmationDialog.componentInstance.showYesNo = false;
    confirmationDialog.componentInstance.message = message;
    return confirmationDialog;
  }
}
