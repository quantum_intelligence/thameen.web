import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CustomAuthService} from '../anonymous/networks/custom.auth.service';
import {UserSettingService} from './user.setting.service';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authService: CustomAuthService , private usersettingService: UserSettingService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // add authorization header with jwt token if available
    let userToken = this.authService.getUserToken();
    if (userToken) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${userToken}` ,
          'Accept-Language' : this.usersettingService.getLang()
        }
      });
    }

    return next.handle(request);
  }


}
