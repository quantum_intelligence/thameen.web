import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {

  constructor() {

  }
  //move code to app comp for generic work

  public scrollTop(){
    const contentContainer = document.querySelector('.mat-sidenav-content') || window;
    contentContainer.scroll(0 ,0);
  }
}
