import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserSettingService {

  constructor() { }

  changeLang(lang:string){
    localStorage.setItem("lang" , lang);
  }

  getLang(){
    if(localStorage.getItem("lang")){
      return localStorage.getItem("lang");
    }
    return "en";
  }

}
