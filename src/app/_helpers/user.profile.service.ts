import { Injectable } from '@angular/core';
import {AccountModel} from '../pages/account/networks/account.model';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor() { }

  saveProfile(profile:AccountModel) {
    localStorage.removeItem("profile");
    localStorage.setItem("profile" , JSON.stringify(profile));
  }

  getProfile(){
    return JSON.parse(localStorage.getItem('profile'));
  }
}
