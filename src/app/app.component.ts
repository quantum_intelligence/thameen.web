import {Component} from '@angular/core';
import {UserSettingService} from './_helpers/user.setting.service';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'motamarat';

  constructor(public userSettingService: UserSettingService , translateService: TranslateService){
    translateService.setDefaultLang(this.userSettingService.getLang());
  }

}
