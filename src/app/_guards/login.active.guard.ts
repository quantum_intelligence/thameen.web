import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {CustomAuthService} from '../anonymous/networks/custom.auth.service';


@Injectable({
  providedIn: 'root'
})
export class LoginActiveGuard implements CanActivate {

  constructor(private authservice: CustomAuthService , private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
   if(!this.authservice.islogedIn()){
     this.router.navigate(['login']);
     return false;
   }
    return true;
  }
}
